============================
 SERV-0001: Initial Bringup
============================
:Author:
  * **A. Wilcox** <awilfox -at- adelielinux>
:Status:
  Draft
:Copyright:
  © 2019 Adélie Linux Team.  CC-BY-SA-4.0 license.




Introduction
============

This document describes a Standard Operating Procedure for the Adélie Linux
distribution's infrastructure.  To propose a change to this SOP, please
contact the Adélie Linux Infrastructure Group via the adelie-infra@ mailing
list.




1. Packages
===========

1. Servers **must** use the ``adelie-base-posix`` package as their anchor.

Justification: This ensures that any and all POSIX shell scripts will run.
Additionally, this ensures that a ``cron`` daemon, ``utmpx`` functionality,
and the ``vim`` text editor will always be avaiable.

2. Servers **must** use the ``dash-binsh`` package as ``/bin/sh`` provider.

Justification: This ensures a minimal ``/bin/sh`` with a smaller attack surface
and faster boot times.

3. Servers **must** use the ``s6-linux-init`` package as ``/sbin/init``.

Justification: Better maintained and smaller attack surface compared to
``sysvinit``.

4. Servers that are intended to have a network connection **must** have the
   ``netifrc`` and ``iproute2`` packages installed.

Justification: These are the standard network configuration packages for
Adélie Linux.  The ``iproute2`` package also adds more feature support for
``netifrc``, including Hetzner-style IPv6 default routes.

5. Servers that are intended to have a network connection **must** have the
   ``openssh`` package installed.

Justification: This ensures that the server can be remotely configured and
administrated.

6. Servers **must** have the following packages available for introspection
   and issue solving:

   * ``gdb``

   * ``lsof``

   * ``musl-dbg``

   * ``strace``

   * ``tmux``

Justification: This makes diagnosing and troubleshooting issues easier and
faster, ensuring higher availability.

7. Servers that are intended to have a network connection **must** have the
   following packages available for introspection and issue solving:

   * ``iputils``

   * ``s6-dns``

   * ``traceroute``

Justification: This makes diagnosing and troubleshooting issues easier and
faster, ensuring higher availability.




2. Networking
=============

1. Servers that are intended to be accessed publicly **must** have a record
   added in the DNS.

Justification: Servers that are publicly accessed will have IP addresses
published anyway.  Ensuring that they have records added in the DNS eases
administration.




3. Configuration
================

1. If appropriate, networking must be configured.

Files: /etc/conf.d/net, /etc/hostname, /etc/resolv.conf.

Justification: Improper or missing network configuration may cause the server
to be "lost", requiring manual intervention from the hosting party.

2. OpenRC must be configured by installing the default runlevels from
   ``/usr/share/openrc/runlevels`` into ``/etc/runlevels``.

Files: /etc/runlevels/.

Justification: The server cannot be booted if OpenRC is not configured.

3. Services must be added to the default OpenRC runlevel.

Files: /etc/init.d/net.*, /etc/init.d/sshd, /etc/init.d/sysklogd.

Justification: Networking must be properly configured as per point 1 of this
section.  OpenSSH must be available as per point 5 of Packaging.  A logging
daemon must be running to ensure issues may be easily diagnosed.

4. User accounts must be created for every member of the Infrastructure Group.

Files: /home/ * /.ssh/authorized_keys.

Justification: All members of the Infrastructure Group must be able to
administrate the server.

5. Configuration files from the `Standard Server Template`__ must be installed.

Justification: The Standard Server Template contains configuration necessary
for all systems in the Adélie Linux infrastructure.

__ https://code.foxkit.us/adelie-infra/server-template/
