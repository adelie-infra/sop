=============================================
 README for Adélie Linux Infrastructure SOPs
=============================================
:Authors:
  * **A. Wilcox**, Distro Lead
  * **Adélie Linux Infrastructure Team**, teaming the infrastructure
:Status:
  Production
:Copyright:
  © 2019 Adélie Linux Team.  CC-BY-SA-4.0 license.




Introduction
============

This repository contains the standard operating procedures (or SOPs) for the
Adélie Linux distribution's infrastructure.


Licenses
`````````
Unless otherwise noted, all SOPs are licensed under the Creative Commons
Attribution-ShareAlike 4.0 (CC-BY-SA-4.0) license.




Contents
========

servers/
````````
Procedures related to servers.


social/
```````
Procedures related to the people running the servers.
